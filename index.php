<?php


// trait

    trait Binatang
    {
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;
        public function atraksi()
        {
            echo "<br>";
            echo "$this->nama sedang $this->keahlian <br> ";
        }
    }




    trait Fight
    {
        public $attackPower;
        public $defencePower;
        public function serang($BinatangDiserang)
        {
            echo "<br>";
            echo "$this->nama sedang menyerang $BinatangDiserang->nama <br> ";
            echo "<br>";
            $this->diserang($BinatangDiserang);
        }
        public function diserang($BinatangDiserang)
        {
            echo "$BinatangDiserang->nama sedang di serang! <br> ";
            echo "<br>";
            return $BinatangDiserang->darah -= ($this->attackPower/$BinatangDiserang->defencePower);
        }
    }



// Class



    class Elang
    {
        use Binatang;
        use Fight;
        public function __construct($nama) {
            $this->jumlahKaki = 2;
            $this->keahlian = "terbang tinggi";
            $this->attackPower = 10;
            $this->defencePower = 5;
            $this->nama = $nama;
        }
        public function getInfoBinatang()
        {
            
            echo "<br>";
            echo "Nama: $this->nama <br> Keahlian: $this->keahlian <br> Attack Power: $this->attackPower <br> Defence Power: $this->defencePower <br> Darah: $this->darah";
            echo "<br>" ;
            
        }
    }
    class Harimau
    {
        use Binatang;
        use Fight;
        public function __construct($nama) {
            $this->jumlahKaki = 4;
            $this->keahlian = "lari cepat";
            $this->attackPower = 7;
            $this->defencePower = 8;
            $this->nama = $nama;
        }
        public function getInfoBinatang()
        {
            
            echo "<br>";
            echo "Nama: $this->nama <br> Keahlian: $this->keahlian <br> Attack Power: $this->attackPower <br> Defence Power: $this->defencePower <br> Darah: $this->darah";
            echo "<br>";
            
        }
    }


                            // OUTPUT

    $elang1 = new Elang("Elang garut");
    $harimau1 = new Harimau("harimau Surabaya");

    $elang1->atraksi();

    $elang1->serang($harimau1);
    $elang1->serang($harimau1);
    $elang1->serang($harimau1);
    $harimau1->getInfoBinatang();
    $elang1->getInfoBinatang();
